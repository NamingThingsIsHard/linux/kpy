A reimplementation of `cp` in rust.

```
USAGE:
    kpy [FLAGS] [OPTIONS] <paths>...

FLAGS:
    -h, --help             Prints help information
    -l, --link             Make hard links instead of copying
    -s, --symbolic-link    Make symbolic links instead of copying
    -V, --version          Prints version information

OPTIONS:
        --parents-stripped <parents-stripped>
            Remove X components from the parent and tack onto destination (use as prefix)


ARGS:
    <paths>...    FILE DIRECTORY | DIRECTORY DIRECTORY | FILE1 FILE2 ... DIRECTORY

Process finished with exit code 0
```

# Why?

I needed `--parents-stripped` in the original `cp` but couldn't find it.
It's something I use more often and I don't want to keep writing a script with `awk`, `sed`, `cut` or whatever
 in order to achieve what a CLI option could do.
Additionally, it has a limited scope and is a nice way to learn rust.

# Dev

Install [Rust](https://www.rust-lang.org/learn/get-started) (which will include `cargo`).

## Build

`cargo build --bin kpy`

## Run

`cargo run kpy`
