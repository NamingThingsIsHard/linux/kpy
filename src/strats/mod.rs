//! The strategies that will be used by the [`crate::copier::Copier`] to change its behavior
//!  before, during, and after copying data around.

pub mod build_destination;
pub mod copy;
pub mod prep_destination;
