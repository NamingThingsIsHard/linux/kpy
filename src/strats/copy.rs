use std::error::Error;
use std::fs::{copy, hard_link};
use std::os::unix::fs::symlink;
use std::path::Path;

use log::debug;

/// A strategy to define how files will be copied from one place to another
pub trait CopyStratTrait {
    fn run(&self, source: &Path, destination: &Path) -> Result<(), Box<dyn Error>>;
}

/// The straight forward strat of really copying the file
pub struct FileCopyStrat {}

impl CopyStratTrait for FileCopyStrat {
    fn run(&self, source: &Path, destination: &Path) -> Result<(), Box<dyn Error>> {
        copy(source, destination)?;
        Ok(())
    }
}

/// A "copy" strategy that actually creates a hard link to the source's content
///
/// Wikipedia provides a good definition of what [hard-links] are.
///
/// [hard-links]: https://en.wikipedia.org/wiki/Hard_link
pub struct FileHardlinkStrat {}

impl CopyStratTrait for FileHardlinkStrat {
    fn run(&self, source: &Path, destination: &Path) -> Result<(), Box<dyn Error>> {
        hard_link(source, destination)?;
        debug!("Linked {:?} to {:?}", destination, source);
        Ok(())
    }
}

/// A "copy" strategy that actually creates a symlink to the source
pub struct FileSymlinkStrat {}

impl CopyStratTrait for FileSymlinkStrat {
    fn run(&self, source: &Path, destination: &Path) -> Result<(), Box<dyn Error>> {
        symlink(source, destination)?;
        debug!("Linked {:?} to {:?}", destination, source);
        Ok(())
    }
}
