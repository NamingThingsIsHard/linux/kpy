extern crate clap;
extern crate dircpy_stable;
extern crate log;
extern crate simple_logger;
extern crate strum;
extern crate strum_macros;
extern crate walkdir;

use log::*;
use simple_logger::SimpleLogger;

use kpy::app::{get_app, get_opts};
use kpy::copier;

fn main() {
    SimpleLogger::new()
        .with_level(log::LevelFilter::Debug)
        .init()
        .expect("Couldn't create simple logger");
    let opts = get_opts();
    let app = get_app(&opts);
    let matches = app.get_matches();
    if let Err(error) = copier::handle_args(&matches, &opts) {
        error!("{}", error);
    }
}
